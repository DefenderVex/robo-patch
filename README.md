# Robomaster S1 SDK Patch #

This is a patcher for the DJI RoboMaster S1 to enable the SDK included in their other RoboMaster model the EP.
It was built and tested on a brand new unit running the 0.06.0500 build on the 25th of November 2021.

This process was designed to be done with the RoboMaster S1 connected to a router.

Everything included here is based on work by u/BrunoGAlbuquerque on reddit
The post can be found here: https://www.reddit.com/r/RobomasterS1/comments/lwx45c/robomaster_s1_sdk_hack/ as of the date above.

I tinkered with this method after I had issues with both getting the RoboMaster to connect over USB and load the patch on boot correctly.

While I'm not completely sure why I had these problems my best guess is its firmware related or a result of how new my unit is.

### How To Install ###

To start you will need to have a copy of the Android SDK Platform Tools downloaded and extracted somewhere on your PC
which can be found at this link: https://developer.android.com/studio/releases/platform-tools as of the date above.

To make things easier you will also need RoboMaster installed on your PC.
It can be downloaded here: https://www.dji.com/nz/downloads/softwares/robomaster-win as of the date above.

* Next you want to place the entire sdk_patch folder in the Platform Tools folder along with install.bat

* Copy the contents of enable_adb.py_ and run it in the LABS window in RoboMaster as Python.

* Run the install.bat file and when prompted enter the IP outputted to the console inside RoboMaster.
* The batch script will walk you though the installation from there.
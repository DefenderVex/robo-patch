@echo off
cls

echo Killing any existing ADB server, any following errors can be ignored.
adb.exe kill-server

echo.

set /p addr="Enter Robomaster S1 IP Address: "

echo.

adb.exe connect %addr%

echo.

echo If a connection was established hit enter to continue, otherwise close this window.

echo.

pause

adb.exe shell rm -rf /data/dji_scratch/sdk

echo.

adb.exe push sdk_patch/dji_scratch/sdk /data/dji_scratch/.

echo.

adb.exe push sdk_patch/dji_scratch/bin/dji_scratch.py /data/dji_scratch/bin/.

echo.

adb.exe push sdk_patch/patch /data/.

echo.

adb.exe shell chmod 755 /data/patch/dji_hdvt_uav

echo.

echo Upload complete, continue to reboot RoboMaster S1 or close this window.
echo You will hear an additional chime approximately five seconds after the first boot chime if the patch was successful.

echo.

pause

adb.exe shell reboot

echo.

echo The RoboMaster S1 has now been rebooted and installation is complete.
echo Please note this patch does NOT automatically start ADB on startup.

echo.

echo A useful thing to do may be to set the intital adb python script as the Autonomous Program in RoboMaster.

echo.

pause